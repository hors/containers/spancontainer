# LICENSE: https://spdx.org/licenses/BSD-2-Clause.html
#
# Visit https://gitlab.com/hors/containers/spancontainer for more information.

from sortedcontainers import SortedDict
from typing import Any, Iterable, List, Optional, Set


class SpanContainer:
    def __init__(self):
        # Contains non-overlapping buckets that contain spans.
        #
        # Maps position: float to Set[Tuple[start: float, end: float, Any]]
        #
        # If pos exists in self._spans, and next_pos is the next key
        # in self._spans. I.e. there is no key x in self._spans which is
        # greater than pos but smaller than next_pos.
        # Then, self._spans[pos] is a set that contains spans that hold
        # [pos, next_pos) inside.

        self._spans = SortedDict()

    def _create_span(self, pos: float):
        if pos in self._spans:
            return
        ind = self._spans.bisect_left(pos)
        if ind == 0 or ind == len(self._spans):
            self._spans[pos] = set()
            return
        existing_pos, tuple_set = self._spans.peekitem(ind - 1)
        assert existing_pos < pos
        self._spans[pos] = set(tuple_set)
        assert self._spans.peekitem(ind)[0] == pos

    def add(self, start: float, end: float, obj: Any):
        # Add a span for obj into range [start, end).
        #
        # Note: end is an exclusive range, so the point at end does not belong
        # into the span.
        if start >= end:
            return

        self._create_span(start)
        self._create_span(end)

        start_ind = self._spans.index(start)
        end_ind = self._spans.index(end)
        assert start_ind < end_ind

        tuple_to_insert = (start, end, obj)

        for ind in range(start_ind, end_ind):
            unused_pos, tuple_set = self._spans.peekitem(ind)
            tuple_set.add(tuple_to_insert)

    def clear(self):
        # Remove all spans from the container
        self._spans.clear()

    def discard(self, start: float, end: float, obj: Any):
        if start >= end:
            return

        try:
            start_ind = self._spans.index(start)
            end_ind = self._spans.index(end)
        except ValueError:
            # The span is not in the list, because there are no span sets
            # at either start or end.
            return
        assert start_ind < end_ind

        tuple_to_discard = (start, end, obj)

        for ind in range(start_ind, end_ind):
            unused_pos, tuple_set = self._spans.peekitem(ind)
            tuple_set.discard(tuple_to_discard)

        # Remove empty tuple sets in range [start, end].
        #
        # TODO: Clean this up.
        ind = start_ind
        if ind > 0:
            tuple_set = self._spans.peekitem(ind - 1)[1]
            if len(tuple_set) == 0:
                ind -= 1

        discard_end_ind = end_ind
        while ind < discard_end_ind and (ind + 1) < len(self._spans):
            cur_tuple_set = self._spans.peekitem(ind)[1]
            if len(cur_tuple_set) == 0:
                next_pos, next_tuple_set = self._spans.peekitem(ind + 1)
                if len(next_tuple_set) == 0:
                    self._spans.pop(next_pos)
                    discard_end_ind -= 1
                else:
                    ind += 1
            else:
                ind += 1

    def _get_span_ind(self, pos: float) -> Optional[int]:
        ind = self._spans.bisect_left(pos)
        if ind == len(self._spans):
            return None
        existing_pos = self._spans.peekitem(ind)[0]
        assert pos <= existing_pos
        if pos == existing_pos:
            return ind
        if ind == 0:
            return None
        return ind - 1

    def _get_range_iterator(self, start: float, end: float) -> Iterable:
        if len(self._spans) == 0:
            return tuple()
        start_ind = self._get_span_ind(start)
        if start_ind is None:
            if start < self._spans.peekitem(0)[0]:
                start_ind = 0
            else:
                start_ind = len(self._spans)
        end_ind = self._get_span_ind(end)
        if end_ind is None:
            if end < self._spans.peekitem(0)[0]:
                end_ind = 0
            else:
                end_ind = len(self._spans)
        else:
            end_ind += 1
        return range(start_ind, end_ind)

    def get_containing(self, pos: float) -> Set:
        """Get spans which contain the given pos."""
        ind = self._get_span_ind(pos)
        if ind is None:
            return set()
        return self._spans.peekitem(ind)[1]

    def get_containing_objects(self, pos: float) -> Set:
        """Same as get_containing() but returns only the objects for spans"""
        return self._get_objects(self.get_containing(pos))

    def get_overlapping(self, start: float, end: float) -> Set:
        """Return spans that are overlapping the range [start, end)."""
        spans = set()
        if start >= end:
            return spans
        for ind in self._get_range_iterator(start, end):
            for span_tuple in self._spans.peekitem(ind)[1]:
                # If span overlaps, then either the start point of the span
                # is inside the range, or the start point of the range is
                # inside the span.
                if (span_tuple[0] >= start and span_tuple[0] < end) or (
                        start >= span_tuple[0] and start < span_tuple[1]):
                    spans.add(span_tuple)

        return spans

    def _get_objects(self, span_tuples) -> Set:
        """Returns only the objects from given span_tuples"""
        objects = set()
        for span_tuple in span_tuples:
            objects.add(span_tuple[2])
        return objects

    def get_overlapping_objects(self, start: float, end: float) -> Set:
        """Same as get_overlapping() but returns only the objects for spans"""
        return self._get_objects(self.get_overlapping(start, end))

    def get_starting(self, start: float, end: float) -> Set:
        """Return spans whose starting point is in the range [start, end)."""
        spans = set()
        if start >= end:
            return spans
        for ind in self._get_range_iterator(start, end):
            for span_tuple in self._spans.peekitem(ind)[1]:
                if span_tuple[0] >= start and span_tuple[0] < end:
                    spans.add(span_tuple)
        return spans

    def get_starting_objects(self, start: float, end: float) -> Set:
        """Same as get_starting() but returns only the objects for spans"""
        return self._get_objects(self.get_starting(start, end))

    def get_subsets(self, start: float, end: float) -> Set:
        """Return spans that are subsets of [start, end)."""
        spans = set()
        if start >= end:
            return spans
        for ind in self._get_range_iterator(start, end):
            for span_tuple in self._spans.peekitem(ind)[1]:
                if span_tuple[0] >= start and span_tuple[0] < end and (
                        span_tuple[1] <= end):
                    spans.add(span_tuple)
        return spans

    def get_subsets_objects(self, start: float, end: float) -> Set:
        """Same as get_subsets() but returns only the objects for spans"""
        return self._get_objects(self.get_subsets(start, end))

    def get_supersets(self, start: float, end: float) -> Set:
        """Return spans that are supersets of [start, end)."""
        spans = set()
        if start >= end:
            return spans
        for ind in self._get_range_iterator(start, end):
            for span_tuple in self._spans.peekitem(ind)[1]:
                if span_tuple[0] <= start and span_tuple[1] >= end:
                    spans.add(span_tuple)
        return spans

    def get_supersets_objects(self, start: float, end: float) -> Set:
        """Same as get_supersets() but returns only the objects for spans"""
        return self._get_objects(self.get_supersets(start, end))

    def remove(self, start: float, end: float, obj):
        """Removes the given span.

        Raises ValueError if the span does not exist.
        """
        if (start, end, obj) not in self:
            raise ValueError('({}, {}, {}) not in SpanContainer'.format(
                start, end, obj))
        self.discard(start, end, obj)

    def objects(self) -> List:
        return span_container_objects_view(self)

    def spans(self) -> List:
        return span_container_spans_view(self)

    def _get_objects_list(self) -> List:
        object_list = []
        object_set = set()
        for span_tuple_set in self._spans.values():
            for span_tuple in span_tuple_set:
                obj = span_tuple[2]
                if obj not in object_set:
                    object_set.add(obj)
                    object_list.append(obj)
        return object_list

    def get_num_objects(self) -> int:
        num = 0
        object_set = set()
        for span_tuple_set in self._spans.values():
            for span_tuple in span_tuple_set:
                obj = span_tuple[2]
                if obj not in object_set:
                    object_set.add(obj)
                    num += 1
        return num

    def __contains__(self, span_tuple):
        return span_tuple in self.get_containing(span_tuple[0])

    def __iter__(self):
        """Returns span tuples ordered by the start pos"""
        spans = set()
        spans_list = []
        for span_tuple_set in self._spans.values():
            for span_tuple in span_tuple_set:
                if span_tuple not in spans:
                    spans_list.append(span_tuple)
                    spans.add(span_tuple)
        return iter(spans_list)

    def __len__(self):
        spans = set()
        for span_tuple_set in self._spans.values():
            spans.update(span_tuple_set)
        return len(spans)


class span_container_objects_view:
    def __init__(self, sc: SpanContainer):
        self.sc = sc

    def __iter__(self):
        return iter(self.sc._get_objects_list())

    def __len__(self):
        return self.sc.get_num_objects()

    def __str__(self):
        objects = self.sc._get_objects_list()
        return 'span_container_objects_view({})'.format(str(objects))


class span_container_spans_view:
    def __init__(self, sc: SpanContainer):
        self.sc = sc

    def __iter__(self):
        return iter(self.sc)

    def __len__(self):
        return len(self.sc)

    def __str__(self):
        spans = list(self.sc)
        return 'span_container_spans_view({})'.format(str(spans))
