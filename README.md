# spancontainer

spancontainer implements a data structure that stores integer/float ranges
(x, y) that are associated with a hashable object. For example, a time
reservation from start_t to end_t (integers) that is associated with an object
can be stored. The data structure is efficient for querying objects that are
in a given range.

# Copying

The project is distributed under the BSD 2-clause license:

https://spdx.org/licenses/BSD-2-Clause.html

COPYING file contains the license.
