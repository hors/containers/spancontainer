import unittest

from spancontainer import SpanContainer


class A:
    pass


class TestSpanContainer(unittest.TestCase):
    def test_add(self):
        sc = SpanContainer()
        a = A()
        b = A()
        self.assertEqual(sc.get_containing(1), set())
        sc.add(1, 3, a)
        self.assertEqual(sc.get_containing(1), {(1, 3, a), })
        sc.add(2, 3, b)
        self.assertEqual(sc.get_containing(0), set())
        self.assertEqual(sc.get_containing(1), {(1, 3, a), })
        self.assertEqual(sc.get_containing(2), {(1, 3, a), (2, 3, b), })
        self.assertEqual(sc.get_containing(3), set())

        sc = SpanContainer()
        sc.add(1, 3, a)
        sc.add(0, 3, a)

        sc = SpanContainer()
        sc.add(1, 3, a)
        sc.add(1, 3, a)
        self.assertEqual(sc.get_containing(1), {(1, 3, a)})
        self.assertEqual(sc.get_containing(2), {(1, 3, a)})
        self.assertEqual(sc.get_containing(3), set())

        sc = SpanContainer()
        sc.add(1, 3, a)
        sc.add(0, 1, a)
        self.assertEqual(sc.get_containing(0), {(0, 1, a)})
        self.assertEqual(sc.get_containing(1), {(1, 3, a)})
        self.assertEqual(sc.get_containing(2), {(1, 3, a)})
        self.assertEqual(sc.get_containing(3), set())

    def test_clear(self):
        sc = SpanContainer()
        a = A()
        sc.add(1, 3, a)
        assert (1, 3, a) in sc
        sc.clear()
        assert (1, 3, a) not in sc

    def test_discard(self):
        sc = SpanContainer()
        a = A()
        b = A()
        sc.add(1, 3, a)
        sc.add(2, 3, b)
        self.assertEqual(len(sc._spans), 3)
        sc.discard(1, 3, a)
        self.assertEqual(len(sc._spans), 3)
        self.assertEqual(sc.get_containing(1), set())
        self.assertEqual(sc.get_containing(2), {(2, 3, b), })
        self.assertEqual(sc.get_containing(3), set())

        sc.discard(2, 3, b)

        # Check removal of empty spans
        self.assertEqual(len(sc._spans), 1)

        # Test that discard should not create empty spans unnecessarily
        sc = SpanContainer()
        sc.add(1, 2, a)
        sc.discard(0, 3, a)
        self.assertNotIn(0, sc._spans)

    def test_empty_container(self):
        sc = SpanContainer()
        self.assertEqual(sc.get_containing(0), set())
        self.assertEqual(sc.get_overlapping(0, 1), set())
        self.assertEqual(sc.get_subsets(0, 1), set())
        self.assertEqual(sc.get_supersets(0, 1), set())

    def test_get_subsets(self):
        sc = SpanContainer()
        a = A()
        b = A()
        c = A()
        sc.add(1, 3, a)
        sc.add(2, 3, b)
        sc.add(3, 4, c)
        self.assertEqual(sc.get_subsets(1, 3), {(1, 3, a), (2, 3, b)})
        self.assertEqual(sc.get_subsets(0, 3), {(1, 3, a), (2, 3, b)})
        self.assertEqual(sc.get_subsets(0, 4), {(1, 3, a), (2, 3, b),
                                                (3, 4, c)})
        self.assertEqual(sc.get_subsets(0, 5), {(1, 3, a), (2, 3, b),
                                                (3, 4, c)})

        self.assertEqual(sc.get_subsets(2.5, 4), {(3, 4, c)})
        self.assertEqual(sc.get_subsets(2.5, 5), {(3, 4, c)})
        self.assertEqual(sc.get_subsets(3, 5), {(3, 4, c)})

        self.assertEqual(sc.get_subsets(0, 1), set())
        self.assertEqual(sc.get_subsets(4, 5), set())

        self.assertEqual(sc.get_subsets_objects(1, 3), {a, b})

    def test_get_supersets(self):
        sc = SpanContainer()
        a = A()
        b = A()
        c = A()
        sc.add(1, 3, a)
        sc.add(2, 3, b)
        sc.add(3, 4, c)
        self.assertEqual(sc.get_supersets(1, 3), {(1, 3, a), })
        self.assertEqual(sc.get_supersets(2, 3), {(1, 3, a), (2, 3, b)})
        self.assertEqual(sc.get_supersets(1.9, 3), {(1, 3, a), })
        self.assertEqual(sc.get_supersets(3, 4), {(3, 4, c), })
        self.assertEqual(sc.get_supersets(0, 3), set())
        self.assertEqual(sc.get_supersets(0, 4), set())
        self.assertEqual(sc.get_supersets(0, 5), set())
        self.assertEqual(sc.get_supersets(2.5, 4), set())
        self.assertEqual(sc.get_supersets(2.5, 5), set())
        self.assertEqual(sc.get_supersets(3, 5), set())
        self.assertEqual(sc.get_supersets(0, 1), set())
        self.assertEqual(sc.get_supersets(4, 5), set())

        self.assertEqual(sc.get_supersets_objects(1, 3), {a, })

    def test_get_overlapping(self):
        sc = SpanContainer()
        a = A()
        b = A()
        c = A()
        sc.add(1, 3, a)
        sc.add(2, 3, b)
        sc.add(3, 4, c)
        self.assertEqual(sc.get_overlapping(1, 3), {(1, 3, a), (2, 3, b)})
        self.assertEqual(sc.get_overlapping(3, 4), {(3, 4, c), })
        self.assertEqual(sc.get_overlapping(0, 3), {(1, 3, a), (2, 3, b)})
        self.assertEqual(sc.get_overlapping(0, 4), {(1, 3, a), (2, 3, b),
                                                    (3, 4, c)})
        self.assertEqual(sc.get_overlapping(0, 5), {(1, 3, a), (2, 3, b),
                                                    (3, 4, c)})

        self.assertEqual(sc.get_overlapping(2.5, 4), {(1, 3, a), (2, 3, b),
                                                      (3, 4, c)})
        self.assertEqual(sc.get_overlapping(2.5, 5), {(1, 3, a), (2, 3, b),
                                                      (3, 4, c)})
        self.assertEqual(sc.get_overlapping(3, 5), {(3, 4, c)})

        self.assertEqual(sc.get_overlapping(0, 1), set())
        self.assertEqual(sc.get_overlapping(4, 5), set())

        self.assertEqual(sc.get_overlapping(1.1, 1.9), {(1, 3, a), })

        self.assertEqual(sc.get_overlapping_objects(1, 3), {a, b})

    def test_get_containing(self):
        sc = SpanContainer()
        a = A()
        sc.add(0, 1, a)
        sc.add(2, 3, a)
        self.assertEqual(sc.get_containing(0), set([(0, 1, a)]))
        self.assertEqual(sc.get_containing(1), set())
        self.assertEqual(sc.get_containing(2), set([(2, 3, a)]))
        self.assertEqual(sc.get_containing(3), set())

        self.assertEqual(sc.get_containing_objects(0), set([a]))

    def test_get_starting(self):
        sc = SpanContainer()
        a = A()
        b = A()
        c = A()
        sc.add(1, 3, a)
        sc.add(2, 3, b)
        sc.add(3, 4, c)
        self.assertEqual(sc.get_starting(1, 3), {(1, 3, a), (2, 3, b)})
        self.assertEqual(sc.get_starting(3, 4), {(3, 4, c), })
        self.assertEqual(sc.get_starting(0, 3), {(1, 3, a), (2, 3, b)})
        self.assertEqual(sc.get_starting(0, 4), {(1, 3, a), (2, 3, b),
                                                 (3, 4, c)})
        self.assertEqual(sc.get_starting(0, 5), {(1, 3, a), (2, 3, b),
                                                 (3, 4, c)})
        self.assertEqual(sc.get_starting(2.5, 4), {(3, 4, c)})
        self.assertEqual(sc.get_starting(2.5, 5), {(3, 4, c)})
        self.assertEqual(sc.get_starting(3, 5), {(3, 4, c)})
        self.assertEqual(sc.get_starting(0, 1), set())
        self.assertEqual(sc.get_starting(4, 5), set())
        self.assertEqual(sc.get_starting(1.1, 1.9), set())

        self.assertEqual(sc.get_starting_objects(1, 3), {a, b})

    def test_iter(self):
        sc = SpanContainer()
        a = A()
        b = A()
        self.assertEqual(list(sc), [])
        sc.add(0, 1, a)
        sc.add(1, 2, a)
        sc.add(2, 3, b)
        self.assertEqual(list(sc), [(0, 1, a), (1, 2, a), (2, 3, b)])
        self.assertEqual(len(sc), 3)
        sc.add(-1, 0, b)
        self.assertEqual(list(sc),
                         [(-1, 0, b), (0, 1, a), (1, 2, a), (2, 3, b)])
        self.assertEqual(len(sc), 4)

    def test_contains(self):
        sc = SpanContainer()
        a = A()
        b = A()
        self.assertEqual(list(sc), [])
        sc.add(0, 1, a)
        sc.add(1, 2, a)
        sc.add(2, 3, b)
        self.assertTrue((0, 1, a) in sc)
        self.assertTrue((1, 2, a) in sc)
        self.assertTrue((2, 3, b) in sc)
        self.assertTrue((2, 3, a) not in sc)

    def test_remove(self):
        sc = SpanContainer()
        a = A()
        sc.add(0, 1, a)
        sc.remove(0, 1, a)
        self.assertFalse((0, 1, a) in sc)
        self.assertRaises(ValueError, sc.remove, 0, 1, a)

    def test_view(self):
        sc = SpanContainer()
        a = A()
        b = A()
        c = A()
        sc.add(0, 1, a)
        sc.add(1, 2, a)
        sc.add(2, 3, b)
        objects_view = sc.objects()
        spans_view = sc.spans()
        self.assertEqual(len(objects_view), 2)
        self.assertEqual(set(objects_view), set([a, b]))
        self.assertEqual(len(spans_view), 3)
        self.assertEqual(set(spans_view),
                         set([(0, 1, a), (1, 2, a), (2, 3, b)]))
        sc.add(3, 4, c)
        self.assertEqual(len(objects_view), 3)
        self.assertEqual(set(objects_view), set([a, b, c]))
        self.assertEqual(len(spans_view), 4)
        self.assertEqual(set(spans_view),
                         set([(0, 1, a), (1, 2, a), (2, 3, b), (3, 4, c)]))


if __name__ == '__main__':
    unittest.main()
